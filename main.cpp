#include <SFML/Graphics.hpp>
#include "Sassy/Sassy.h"

int main(int argc, char** argv)
{
	sf::RenderWindow window(sf::VideoMode(800, 600), "Sassy", sf::Style::Close);

	sf::Font font;
	font.loadFromFile("consolas.ttf");

	sy::GUI gui;
	gui.addElement("demo", new sy::Window(sf::Vector2f(400, 300), sf::Vector2f(300, 300), font, "demo"));
	sy::Window* demo = (sy::Window*)gui.getElement("demo");
	demo->focused = true;

	demo->addElement("label", new sy::Label(sf::Vector2f(0, -100), font, "set this with the text box"));

	demo->addElement("button", new sy::Button(sf::Vector2f(75, -50), sf::Vector2f(64, 32), font, "set"));
	sy::Button* button = (sy::Button*)demo->getElement("button");

	demo->addElement("textbox", new sy::TextBox(sf::Vector2f(-75, -50), sf::Vector2f(128, 32), font, 14));

	demo->addElement("label2", new sy::Label(sf::Vector2f(0, 30), font, "cool check box and slider"));
	demo->addElement("checkbox", new sy::CheckBox(sf::Vector2f(0, 75), sf::Vector2f(32, 32)));
	demo->addElement("slider", new sy::Slider(sf::Vector2f(0, 125), sf::Vector2f(128, 24)));

	while(window.isOpen())
	{
		sf::Event event;
		while(window.pollEvent(event))
		{
			gui.processElementEvents(event);
			if(event.type == sf::Event::Closed)
			{
				window.close();

			}

		}

		if(button->isPressed())
		{
			sy::Label* label = (sy::Label*)demo->getElement("label");
			sy::TextBox* textbox = (sy::TextBox*)demo->getElement("textbox");
			label->setString(textbox->str);

		}

		window.clear(sf::Color::White);

		gui.drawElements(window);

		window.display();

	}

	return 0;

}
