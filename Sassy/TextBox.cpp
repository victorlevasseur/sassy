#include "TextBox.h"

namespace sy
{

TextBox::TextBox(sf::Vector2f pos,
		sf::Vector2f size,
		sf::Font& font,
		unsigned int charSize,
		bool secret) :
			secret(secret)
{
	setPosition(pos);

	box.setSize(size);
	box.setOrigin(size/2.0f);
	box.setFillColor(sf::Color::White);
	box.setOutlineColor(sf::Color::Black);
	box.setOutlineThickness(2.0f);

	cursor.setSize(sf::Vector2f(2.0f, size.y));
	cursor.setOrigin(cursor.getSize()/2.0f);
	cursor.setFillColor(sf::Color::Black);
	cursor.setPosition(-size.x/2, 0);
	cursorVisible = true;

	text.setFont(font);
	text.setColor(sf::Color::Black);
	text.setCharacterSize(charSize);
	text.setOrigin(0, size.y/2.0f);
	text.setPosition(-size.x/2.0f, 0);

	highlight = box;
	highlight.setFillColor(sf::Color::Transparent);

	entered = false;
	tabbed = false;
	focused = false;
	hovered = false;

}

void TextBox::processEvent(sf::Event event)
{
	if(event.type == sf::Event::MouseMoved)
	{
		highlight.setFillColor(sf::Color::Transparent);
		sf::Vector2f mPos(event.mouseMove.x, event.mouseMove.y);
		sf::FloatRect bounds = box.getGlobalBounds();
		sf::Vector2f pos = getOffset();
		bounds.left += pos.x;
		bounds.top += pos.y;
		hovered = bounds.contains(mPos);
		if(hovered && !focused)
		{
			highlight.setFillColor(theme.highlight);

		}

	}
	else if(event.type == sf::Event::MouseButtonPressed)
	{
		focused = hovered && event.mouseButton.button == sf::Mouse::Left;
		box.setFillColor(theme.primaryDark);
		if(focused)
		{
			highlight.setFillColor(sf::Color::Transparent);
			box.setFillColor(theme.focused);

		}

	}


	if(focused)
	{
		entered = false;
		tabbed = false;
		if(event.type == sf::Event::TextEntered && (event.text.unicode < 128 || event.text.unicode > 159 ))
		{
			if(event.text.unicode == 13)
			{
				entered = true;

			}
			else if(event.text.unicode == 9)
			{
				tabbed = true;

			}
			else if(event.text.unicode == 8)
			{
				if(str.getSize() > 0)
				{
					str.erase(str.getSize()-1);
					
				}
			}
			else
			{
				str += sf::String(event.text.unicode);

			}

		}

	}

}

void TextBox::draw(sf::RenderWindow& window,
			sf::RenderStates states)
{
	if(secret)
	{
		std::string fake;
		for(unsigned int i = 0; i < str.getSize(); i++)
		{
			fake += "*";

		}
		text.setString(fake);

	}
	else
	{
		text.setString(str);

	}

	cursor.setPosition(-box.getSize().x/2.0f + text.getGlobalBounds().width + 2.0f, 0);

	if(blinkTimer.getElapsedTime().asSeconds() >= 0.5)
	{
		cursorVisible = !cursorVisible;
		blinkTimer.restart();

	}

	states.transform *= getTransform();
	window.draw(box, states);
	window.draw(text, states);

	if(cursorVisible && focused)
	{
		window.draw(cursor, states);

	}

	window.draw(highlight, states);

}

bool TextBox::wasEntered()
{
	return entered;

}

void TextBox::setTheme(Theme theme)
{
	box.setFillColor(theme.primaryDark);

	box.setOutlineThickness(theme.thickness);
	box.setOutlineColor(theme.outline);

	cursor.setFillColor(theme.secondary);

	highlight.setOutlineThickness(theme.thickness);
	highlight.setOutlineColor(theme.outline);

	if(theme.hasFont())
	{
		text.setFont(*theme.font);

	}

	Element::setTheme(theme);

}

}
