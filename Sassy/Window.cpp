#include "Window.h"

namespace sy
{

Window::Window(sf::Vector2f pos,
		sf::Vector2f size,
		sf::Font& font,
		const sf::String &str,
		unsigned int charSize)
{
	setPosition(pos);

	base.setSize(size);
	base.setOrigin(size/2.0f);
	base.setOutlineThickness(1.0f);
	base.setOutlineColor(sf::Color::Black);

	bar.setSize(sf::Vector2f(size.x, 20));
	bar.setOrigin(size.x/2.0f, size.y/2.0f);
	bar.setOutlineThickness(1.0f);
	bar.setOutlineColor(sf::Color::Black);

	text = sf::Text(str, font, charSize);
	text.setColor(sf::Color::Black);
	text.setOrigin(size.x/2.0f, size.y/2.0f);

	highlight = base;
	highlight.setFillColor(sf::Color::Transparent);

	hovered = false;
	focused = false;
	hovered2 = false;
	dragging = false;

}

bool Window::addElement(std::string name,
		Element* element)
{
	element->parent = this;
	return GUI::addElement(name, element);

}

void Window::processEvent(sf::Event event)
{
	if(event.type == sf::Event::MouseMoved)
	{
		highlight.setFillColor(sf::Color::Transparent);
		sf::Vector2f mPos(event.mouseMove.x, event.mouseMove.y);
		if(dragging)
		{
			setPosition(mPos+dragOffset);

		}

		sf::FloatRect bounds = base.getGlobalBounds();
		sf::Vector2f pos = getOffset();
		bounds.left += pos.x;
		bounds.top += pos.y;
		hovered = bounds.contains(mPos);
		if(hovered && !focused)
		{
			highlight.setFillColor(theme.highlight);

		}

		sf::FloatRect barBounds = bar.getGlobalBounds();
		barBounds.left += pos.x;
		barBounds.top += pos.y;
		hovered2 = barBounds.contains(mPos);

	}
	else if(event.type == sf::Event::MouseButtonPressed)
	{
		bool mouseLeft = event.mouseButton.button == sf::Mouse::Left;
		focused = hovered && mouseLeft;
		bar.setFillColor(theme.primaryDark);
		if(focused)
		{
			bar.setFillColor(theme.focused);

		}

		dragging = hovered2 && mouseLeft;
		if(dragging)
		{
			sf::Vector2f mPos(event.mouseButton.x, event.mouseButton.y);
			dragOffset = getPosition() -  mPos;

		}

	}
	else if(event.type == sf::Event::MouseButtonReleased)
	{
		bool mouseLeft = event.mouseButton.button == sf::Mouse::Left;
		if(mouseLeft)
		{
			dragging = false;

		}

	}

	if(focused)
	{
		processElementEvents(event);

	}

}

void Window::draw(sf::RenderWindow& window,
		sf::RenderStates states)
{
	states.transform *= getTransform();

	window.draw(base, states);
	window.draw(bar, states);
	window.draw(text, states);

	drawElements(window, states);

	window.draw(highlight, states);


}

void Window::setTheme(Theme theme)
{
	base.setFillColor(theme.primary);

	base.setOutlineThickness(theme.thickness);
	base.setOutlineColor(theme.outline);

	bar.setFillColor(theme.primaryDark);

	bar.setOutlineThickness(theme.thickness);
	bar.setOutlineColor(theme.outline);

	if(theme.hasFont())
	{
		text.setFont(*theme.font);

	}

	highlight.setOutlineThickness(theme.thickness);
	highlight.setOutlineColor(theme.outline);

	Element::setTheme(theme);
	setUserTheme(theme);

}

void Window::setUserTheme(Theme theme)
{
	GUI::setUserTheme(theme);

}

}
