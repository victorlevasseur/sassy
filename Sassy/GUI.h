#ifndef GUI_H_
#define GUI_H_

#include "Element.h"

namespace sy
{

class GUI
{
public:
	GUI();

	virtual ~GUI();

	virtual bool addElement(std::string name,
			Element* element);

	Element* getElement(std::string name);

	virtual void processElementEvents(sf::Event event);

	void drawElements(sf::RenderWindow& window,
			sf::RenderStates states = sf::RenderStates::Default);

	void setUserTheme(Theme theme);

	void setFont(sf::Font* font);

	std::vector<std::pair<std::string, Element*> > elements;
	Theme userTheme;

};

}

#endif /* GUI_H_ */
