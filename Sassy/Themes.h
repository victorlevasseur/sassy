#ifndef THEMES_H_
#define THEMES_H_

#include "Theme.h"
#include "Util.h"

namespace sy
{

static Theme royal(NULL, 1, 0x0, 0x673ab7, 0x512da8, 0xffc107, 0xd1c4e9, sf::Color(0, 0, 0, 96));
static Theme royalBorderless(NULL, 0, 0x0, 0x673ab7, 0x512da8, 0xffc107, 0xd1c4e9, sf::Color(0, 0, 0, 96));

static Theme tigerEye(NULL, 1, 0x0, 0xffc107, 0xffa000, 0x795548, 0xffecb3, sf::Color(0, 0, 0, 96));
static Theme tigerEyeBorderless(NULL, 0, 0x0, 0xffc107, 0xffa000, 0x795548, 0xffecb3, sf::Color(0, 0, 0, 96));

static Theme waterMelon(NULL, 1, 0x0, 0x009688, 0x00796b, 0xff4081, 0xb2dfdb, sf::Color(0, 0, 0, 96));
static Theme waterMelonBorderless(NULL, 0, 0x0, 0x009688, 0x00796b, 0xff4081, 0xb2dfdb, sf::Color(0, 0, 0, 96));

static Theme deepSea(NULL, 1, 0x0, 0x3f51b5, 0x303f9f, 0x8bc34a, 0xc5cae9, sf::Color(0, 0, 0, 96));
static Theme deepSeaBorderless(NULL, 0, 0x0, 0x3f51b5, 0x303f9f, 0x8bc34a, 0xc5cae9, sf::Color(0, 0, 0, 96));

}

#endif /* THEMES_H_ */
