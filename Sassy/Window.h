#ifndef WINDOW_H_
#define WINDOW_H_

#include "GUI.h"

namespace sy
{

class Window : public GUI, public Element
{
public:
	Window(sf::Vector2f pos,
			sf::Vector2f size,
			sf::Font& font,
			const sf::String &str,
			unsigned int charSize = 20);

	bool addElement(std::string name,
			Element* element);

	void processEvent(sf::Event event);

	void draw(sf::RenderWindow& window,
			sf::RenderStates states = sf::RenderStates::Default);

	void setTheme(Theme theme);

	void setUserTheme(Theme theme);

	bool hovered;
	bool focused;

	bool hovered2;
	bool dragging;
	sf::Vector2f dragOffset;

	sf::RectangleShape base;
	sf::RectangleShape bar;
	sf::Text text;
	sf::RectangleShape highlight;

};

}

#endif /* WINDOW_H_ */
