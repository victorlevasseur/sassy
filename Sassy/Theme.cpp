#include "Theme.h"
#include "Util.h"

namespace sy
{

Theme::Theme()
{
}

Theme::Theme(sf::Font* font,
		unsigned int thickness,
		sf::Color outline,
		sf::Color primary,
		sf::Color primaryDark,
		sf::Color secondary,
		sf::Color focused,
		sf::Color highlight) :
				font(font),
				thickness(thickness),
				outline(outline),
				primary(primary),
				primaryDark(primaryDark),
				secondary(secondary),
				focused(focused),
				highlight(highlight)
{
}

Theme::Theme(sf::Font* font,
		unsigned int thickness,
		unsigned int outline,
		unsigned int primary,
		unsigned int primaryDark,
		unsigned int secondary,
		unsigned int focused,
		sf::Color highlight) :
				font(font),
				thickness(thickness),
				outline(hexToColor(outline)),
				primary(hexToColor(primary)),
				primaryDark(hexToColor(primaryDark)),
				secondary(hexToColor(secondary)),
				focused(hexToColor(focused)),
				highlight(highlight)
{
}

bool Theme::hasFont()
{
	return font != NULL;

}

}
