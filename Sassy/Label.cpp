#include "Label.h"

namespace sy
{

Label::Label(sf::Vector2f pos,
		sf::Font& font,
		const sf::String &str,
		unsigned int charSize)
{
	setPosition(pos);

	text = sf::Text("", font, charSize);
	text.setColor(sf::Color::Black);
	setString(str);

}

void Label::processEvent(sf::Event event)
{
}

void Label::draw(sf::RenderWindow& window,
		sf::RenderStates states)
{
	states.transform *= getTransform();
	window.draw(text, states);

}

void Label::setString(const sf::String &str)
{
	text.setString(str);
	sf::FloatRect bounds = text.getGlobalBounds();
	text.setOrigin(bounds.width/2.0f, bounds.height/2.0f);

}

void Label::setTheme(Theme theme)
{
	if(theme.hasFont())
	{
		text.setFont(*theme.font);
		setString(text.getString());

	}

	Element::setTheme(theme);

}

}
