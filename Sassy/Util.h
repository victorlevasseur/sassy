#ifndef UTIL_H_
#define UTIL_H_

#include <SFML/Graphics.hpp>

namespace sy
{

sf::Color hexToColor(unsigned int hex);

unsigned int colorToHex(sf::Color color);

}

#endif /* UTIL_H_ */
