#include "Element.h"

namespace sy
{

Element::Element()
{
	parent = NULL;

}

Element::~Element()
{
}

void Element::processEvent(sf::Event event)
{
}

void Element::draw(sf::RenderWindow& window,
		sf::RenderStates states)
{
}

sf::Vector2f Element::getOffset()
{
	if(parent != NULL)
	{
		return getPosition() + parent->getOffset();

	}

	return getPosition();

}

void Element::setTheme(Theme theme)
{
	this->theme = theme;

}

}

