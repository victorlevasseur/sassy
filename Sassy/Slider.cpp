#include "Slider.h"
#include <math.h>

namespace sy
{

Slider::Slider(sf::Vector2f pos,
		sf::Vector2f size,
		float value) :
			value(value)
{
	setPosition(pos);

	line.setSize(sf::Vector2f(size.x, 4));
	line.setOrigin(size.x/2.0f, 2);
	line.setFillColor(sf::Color::Black);

	float handleWidth = fmin(fmax(size.x*0.02, 8), 16);
	handle.setSize(sf::Vector2f(handleWidth, size.y));
	handle.setOrigin(handleWidth/2.0f, size.y/2.0f);
	handle.setOutlineThickness(1.0f);
	handle.setOutlineColor(sf::Color::Black);

	size.x += handleWidth;
	highlight.setSize(size);
	highlight.setOrigin(size/2.0f);
	highlight.setFillColor(sf::Color::Transparent);

	dragging = false;
	hovered = false;

}

void Slider::processEvent(sf::Event event)
{
	if(event.type == sf::Event::MouseMoved)
	{
		highlight.setFillColor(sf::Color::Transparent);
		sf::Vector2f mPos(event.mouseMove.x, event.mouseMove.y);
		sf::Vector2f pos = getOffset();
		if(dragging)
		{

			sf::FloatRect lineBounds = line.getGlobalBounds();
			lineBounds.left += pos.x;
			lineBounds.top += pos.y;

			if(mPos.x < lineBounds.left)
				mPos.x = lineBounds.left;
			else if(mPos.x > lineBounds.left + lineBounds.width)
				mPos.x = lineBounds.left + lineBounds.width;

			mPos.x -= lineBounds.left + lineBounds.width/2.0f;

			handle.setPosition(mPos.x, 0);

			value = (mPos.x/lineBounds.width) + 0.5;

		}

		sf::FloatRect bounds = highlight.getGlobalBounds();

		bounds.left += pos.x;
		bounds.top += pos.y;
		hovered = bounds.contains(mPos);
		if(hovered && !dragging)
		{
			highlight.setFillColor(theme.highlight);

		}

	}
	else if(event.type == sf::Event::MouseButtonPressed)
	{
		bool mouseLeft = event.mouseButton.button == sf::Mouse::Left;
		if(hovered && mouseLeft)
		{
			dragging = true;
			sf::Vector2f mPos(event.mouseButton.x, event.mouseButton.y);
			handle.setFillColor(theme.focused);
			highlight.setFillColor(sf::Color::Transparent);

		}

	}
	else if(event.type == sf::Event::MouseButtonReleased)
	{
		bool mouseLeft = event.mouseButton.button == sf::Mouse::Left;
		if(mouseLeft)
		{
			dragging = false;
			handle.setFillColor(theme.secondary);

		}

	}

}

void Slider::draw(sf::RenderWindow& window,
		sf::RenderStates states)
{
	states.transform *= getTransform();
	window.draw(line, states);
	window.draw(handle, states);
	window.draw(highlight, states);

}

void Slider::setTheme(Theme theme)
{
	line.setOutlineColor(theme.primaryDark);

	handle.setFillColor(theme.secondary);

	handle.setOutlineThickness(theme.thickness);
	handle.setOutlineColor(theme.outline);

	Element::setTheme(theme);

}

}

