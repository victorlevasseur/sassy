#include "GUI.h"

namespace sy
{

GUI::GUI()
{
	userTheme = Theme(NULL);

}

GUI::~GUI()
{
	while(!elements.empty())
	{
		delete elements.begin()->second;
		elements.erase(elements.begin());

	}

}

bool GUI::addElement(std::string name,
		Element* element)
{
	for(unsigned int i = 0; i < elements.size(); i++)
	{
		if(elements[i].first == name)
		{
			return false;

		}

	}

	element->setTheme(userTheme);

	elements.push_back(std::pair<std::string, Element*>(name, element));
	return true;

}

Element* GUI::getElement(std::string name)
{
	for(unsigned int i = 0; i < elements.size(); i++)
	{
		if(elements[i].first == name)
		{
			return elements[i].second;
		}

	}

	return NULL;

}

void GUI::processElementEvents(sf::Event event)
{
	for(unsigned int i = 0; i < elements.size(); i++)
	{
		elements[i].second->processEvent(event);

	}

}

void GUI::drawElements(sf::RenderWindow& window,
		sf::RenderStates states)
{
	for(unsigned int i = 0; i < elements.size(); i++)
	{
		elements[i].second->draw(window, states);

	}

}

void GUI::setUserTheme(Theme theme)
{
	for(unsigned int i = 0; i < elements.size(); i++)
	{
		elements[i].second->setTheme(theme);

	}

	userTheme = theme;

}

void GUI::setFont(sf::Font* font)
{
	userTheme.font = font;
	setUserTheme(userTheme);

}

}

