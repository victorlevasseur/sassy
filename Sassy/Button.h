#ifndef BUTTON_H_
#define BUTTON_H_

#include "Element.h"

namespace sy
{

class Button : public Element
{
public:
	Button(sf::Vector2f pos,
			sf::Vector2f size,
			sf::Font& font,
			const sf::String &str,
			unsigned int charSize = 20);

	void processEvent(sf::Event event);

	void draw(sf::RenderWindow& window,
			sf::RenderStates states = sf::RenderStates::Default);

	bool isPressed();

	void setTheme(Theme theme);

	bool pressed;
	bool hovered;

	sf::RectangleShape base;
	sf::Text text;
	sf::RectangleShape highlight;

};

}

#endif /* BUTTON_H_ */
