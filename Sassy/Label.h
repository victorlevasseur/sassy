#ifndef LABEL_H_
#define LABEL_H_

#include "Element.h"

namespace sy
{

class Label : public Element
{
public:
	Label(sf::Vector2f pos,
			sf::Font& font,
			const sf::String &str,
			unsigned int charSize = 20);

	void processEvent(sf::Event event);

	void draw(sf::RenderWindow& window,
			sf::RenderStates states = sf::RenderStates::Default);

	void setString(const sf::String &str);

	void setTheme(Theme theme);

	sf::Text text;

};

}

#endif /* LABEL_H_ */
