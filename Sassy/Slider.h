#ifndef SLIDER_H_
#define SLIDER_H_

#include "Element.h"

namespace sy
{

class Slider : public Element
{
public:
	Slider(sf::Vector2f pos,
			sf::Vector2f size,
			float value = 50.0f);

	void processEvent(sf::Event event);

	void draw(sf::RenderWindow& window,
			sf::RenderStates states = sf::RenderStates::Default);

	void setTheme(Theme theme);

	float value;
	bool dragging;
	bool hovered;

	sf::RectangleShape line;
	sf::RectangleShape handle;
	sf::RectangleShape highlight;

};

}

#endif /* SLIDER_H_ */
