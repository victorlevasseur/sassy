#ifndef CHECKBOX_H_
#define CHECKBOX_H_

#include "Element.h"

namespace sy
{

class CheckBox : public Element
{
public:
	CheckBox(sf::Vector2f pos,
			sf::Vector2f size,
			bool checked = false);

	void processEvent(sf::Event event);

	void draw(sf::RenderWindow& window,
			sf::RenderStates states = sf::RenderStates::Default);

	void setTheme(Theme theme);

	sf::RectangleShape base;
	sf::RectangleShape check;
	sf::RectangleShape highlight;

	bool checked;
	bool hovered;

};

}

#endif /* CHECKBOX_H_ */
