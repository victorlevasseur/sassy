#ifndef TEXTBOX_H_
#define TEXTBOX_H_

#include "Element.h"

namespace sy
{

class TextBox : public Element
{
public:
    TextBox(sf::Vector2f pos,
            sf::Vector2f size,
            sf::Font& font,
			unsigned int charSize = 20,
			bool secret = false);

    void processEvent(sf::Event event);

    void draw(sf::RenderWindow& window,
                sf::RenderStates states = sf::RenderStates::Default);

    bool wasEntered();

    void setTheme(Theme theme);

    bool focused;
    bool entered;
    bool tabbed;
    bool hovered;
    bool secret;

    sf::String str;
    sf::Text text;
    sf::RectangleShape box;
    sf::RectangleShape highlight;

    sf::RectangleShape cursor;
    bool cursorVisible;
    sf::Clock blinkTimer;

};

}

#endif /* TEXTBOX_H_ */
