#ifndef ELEMENT_H_
#define ELEMENT_H_

#include <SFML/Graphics.hpp>
#include <SFML/System/String.hpp>
#include "Theme.h"

namespace sy
{

class Element : public sf::Transformable
{
public:
	Element();

	virtual ~Element();

	virtual void processEvent(sf::Event event);

	virtual void draw(sf::RenderWindow& window,
			sf::RenderStates states = sf::RenderStates::Default);

	sf::Vector2f getOffset();

	virtual void setTheme(Theme theme);

	Theme theme;
	Element* parent;

};

}

#endif /* ELEMENT_H_ */
