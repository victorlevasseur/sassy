#include "Util.h"

namespace sy
{

sf::Color hexToColor(unsigned int hex)
{
	sf::Color color;
	color.r = ((hex >> 16) & 0xff);
	color.g = ((hex >> 8 ) & 0xff);
	color.b = ((hex) & 0xff);
	return color;

}

unsigned int colorToHex(sf::Color color)
{
	unsigned int hex = 0;
	hex |= (color.r << 16);
	hex |= (color.g << 8);
	hex |= color.b;
	return hex;

}

}
