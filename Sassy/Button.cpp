#include "Button.h"

namespace sy
{

Button::Button(sf::Vector2f pos,
		sf::Vector2f size,
		sf::Font& font,
		const sf::String &str,
		unsigned int charSize)
{
	setPosition(pos);

	base.setSize(size);
	base.setOrigin(size/2.0f);
	base.setOutlineThickness(1.0f);
	base.setOutlineColor(sf::Color::Black);

	text = sf::Text(str, font, charSize);
	text.setColor(sf::Color::Black);
	sf::FloatRect textBounds = text.getLocalBounds();
	text.setOrigin(textBounds.width/2.0f, textBounds.height);

	highlight = base;
	highlight.setFillColor(sf::Color::Transparent);

	pressed = false;
	hovered = false;

}

void Button::processEvent(sf::Event event)
{
	pressed = false;
	if(event.type == sf::Event::MouseMoved)
	{
		highlight.setFillColor(sf::Color::Transparent);
		sf::Vector2f mPos(event.mouseMove.x, event.mouseMove.y);
		sf::FloatRect bounds = base.getGlobalBounds();
		sf::Vector2f pos = getOffset();
		bounds.left += pos.x;
		bounds.top += pos.y;
		hovered = bounds.contains(mPos);
		if(hovered)
		{
			highlight.setFillColor(theme.highlight);

		}

	}
	else if(hovered && event.type == sf::Event::MouseButtonPressed)
	{
		pressed = event.mouseButton.button == sf::Mouse::Left;

	}

}

void Button::draw(sf::RenderWindow& window,
		sf::RenderStates states)
{
	states.transform *= getTransform();
	window.draw(base, states);
	window.draw(text, states);
	window.draw(highlight, states);

}

bool Button::isPressed()
{
	return pressed;

}

void Button::setTheme(Theme theme)
{
	base.setFillColor(theme.secondary);
	base.setOutlineThickness(theme.thickness);
	base.setOutlineColor(theme.outline);

	if(theme.hasFont())
	{
		text.setFont(*theme.font);
		sf::FloatRect textBounds = text.getLocalBounds();
		text.setOrigin(textBounds.width/2.0f, textBounds.height);

	}

	highlight.setOutlineThickness(theme.thickness);
	highlight.setOutlineColor(theme.outline);

	Element::setTheme(theme);

}

}
